package MediaOperations

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

func GetSizeInMB(size float64) float64 {
	return size / 1024
}
func GetSizeInGB(size float64) float64 {
	return size / 1024
}
func GetSizeInKB(len string) float64 {
	size, _ := strconv.ParseFloat(len, 64)
	f := (float64(size) * 1.0 / 1024.0)
	return f
}
func GetURL() string {

	reader := bufio.NewReader(os.Stdin)
	url, _ := reader.ReadString('\n')
	return url
}
func GetTypeAndFormat(s string) (string, string) {
	tmp := strings.Split(s, "/")
	return tmp[0], tmp[1]

}
