package main

import (
	"fmt"
	"log"
	"net/http"
	"projectMediaInfo/MediaOperations"
	"strings"
)

func main() {

	urlStr := MediaOperations.GetURL()
	urlStr = strings.TrimSpace(urlStr)

	responce, err := http.Get(urlStr)
	if err != nil {
		log.Fatal("URL is not valid")
	}
	defer func() {
		responce.Body.Close()
	}()
	typeOfFile, formatOfFile := MediaOperations.GetTypeAndFormat(responce.Header.Get("Content-Type"))
	fmt.Printf("%T\n", responce)
	fmt.Println("File Type-> ", typeOfFile, "\nFile Format->", formatOfFile)
	sizeInKB := MediaOperations.GetSizeInKB(responce.Header.Get("Content-Length"))
	sizeInMB := MediaOperations.GetSizeInMB(sizeInKB)
	sizeInGB := MediaOperations.GetSizeInGB(sizeInMB)
	lastModify := responce.Header.Get("Last-Modified")
	urlActiveFrom := responce.Header.Get("Date")
	expiryOfURL := "NA"
	if t := responce.Header.Get("Expires"); len(t) > 0 {
		expiryOfURL = t
	}
	fmt.Printf("File Size->  %.2f KB,", sizeInKB)
	fmt.Printf("  %.2f MB,", sizeInMB)
	fmt.Printf("  %.2f GB,\n", sizeInGB)
	fmt.Println("Last Modified on-> ", lastModify)
	fmt.Println("Active From -> ", urlActiveFrom)
	fmt.Println("Expire On -> ", expiryOfURL)

	if strings.TrimSpace(typeOfFile) == "image" {

		MediaOperations.ImageOperations(responce)

	} else if strings.TrimSpace(typeOfFile) == "video" {
		MediaOperations.VideoOperations(responce)
	}

	/*
		body, err := ioutil.ReadAll(responce.Body)
		fmt.Println("Body ", body)
		if err != nil {
			log.Fatal("Error   ", err.Error())
		}

		json_map := make(map[string]interface{})
		json.Unmarshal(body, &json_map)
		fmt.Printf("Results: %v\n", json_map)
	*/
	// alternately:
	/*
		foo2 := Foo{}
		getJson(urlStr, &foo2)
		fmt.Println(foo2.Bar)
	*/
}

/*
type Foo struct {
	Bar string
}

var myClient = &http.Client{Timeout: 10 * time.Second}

func getJson(url string, target interface{}) error {
	r, err := myClient.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	e := json.NewDecoder(r.Body).Decode(target)
	fmt.Println(target)
	return e
}
*/
//https://r5---sn-qxaelned.googlevideo.com/videoplayback?expire=1638284790&ei=lumlYeTwKPWN6dsPh7a-4Aw&ip=212.102.57.91&id=o-AAPI-zKnsJ78KMwP66Mbt6YPPRLOH6e-vxYW7gFgicA4&itag=18&source=youtube&requiressl=yes&vprv=1&mime=video%2Fmp4&ns=kaZX6WdfGqoMCaOiPoe3oqIG&gir=yes&clen=152882981&ratebypass=yes&dur=3674.348&lmt=1612777692485280&fexp=24001373,24007246&c=WEB&txp=5430432&n=VzRV6e738LOpBS6A&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cgir%2Cclen%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRQIhAPtbEstEHoHir5zHumgnm_qsCgCdAEXx4BT9CsXGDqLHAiBLDXC5lS6KkaDRMgmEXSvQUaZWtUlfWBCe9WD0l93iSg%3D%3D&rm=sn-n02xgoxufvg3-2gbz76,sn-4g5ek776&req_id=cd2a31dbc82ea3ee&redirect_counter=2&cms_redirect=yes&ipbypass=yes&mh=dT&mip=2402:3a80:1a57:c69b:c9f2:923b:e1cf:d9c7&mm=29&mn=sn-qxaelned&ms=rdu&mt=1638263100&mv=m&mvi=5&pl=48&lsparams=ipbypass,mh,mip,mm,mn,ms,mv,mvi,pl&lsig=AG3C_xAwRgIhAPbOL-p0G9qW6v187KhVcds1emDEsOxy41cAakb7MSMxAiEA7K8BXF3r_RJK8P4NbgOpZ9ORgmDsjVmo_OpMs3VF_MU%3D
//http://i.imgur.com/m1UIjW1.jpg
//https://r3---sn-25glen7l.googlevideo.com/videoplayback?expire=1638454129&ei=EX-oYZ7FGdL5xN8PzI2ewAI&ip=115.85.72.202&id=o-APcxzYAAOLBtbJYaaOaX_6tnKFbmu7KLAjM0U3LSYNJw&itag=18&source=youtube&requiressl=yes&vprv=1&mime=video%2Fmp4&ns=u9JBi4DlhPee8AUr4xbSR8EG&gir=yes&clen=60835409&ratebypass=yes&dur=1049.867&lmt=1527668072731461&fexp=24001373,24007246&c=WEB&n=dLpaQ76aRcKSfg&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cgir%2Cclen%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRgIhAK3AAl-ePyX785WcbjJa83RMlzm0_nlyYWEPDf4Doji2AiEAouktgbEKcgVehGA0nWNd0-W5-FmTY_5IpoclBUvR7vM%3D&rm=sn-poqvn5u-jb3d7s,sn-npole7s&req_id=940eec206d3ea3ee&redirect_counter=2&cms_redirect=yes&ipbypass=yes&mh=1y&mip=2402:8100:2061:753e:d0a9:7909:c1ba:37e6&mm=30&mn=sn-25glen7l&ms=nxu&mt=1638432264&mv=m&mvi=3&pl=48&lsparams=ipbypass,mh,mip,mm,mn,ms,mv,mvi,pl&lsig=AG3C_xAwRAIgRWOnNGQNwgPmPdA-B6seXWdYIY36oeLFID8mWQs4zfQCIHO3lE0DaMQ7NGYPI_--sn6HFdXt88Tp4g27wjXD9gng
//https://r3---sn-q4fl6nle.googlevideo.com/videoplayback?expire=1638630140&ei=nC6rYZ-zK9Hthwb1z4HICw&ip=102.165.16.201&id=o-AGfBtaq-R9SiOB6JytfNUlTn4VEU2GDXt171SaToX0JC&itag=18&source=youtube&requiressl=yes&vprv=1&mime=video%2Fmp4&ns=2vgYySliD10_2UhdX656f0MG&cnr=14&ratebypass=yes&dur=756.599&lmt=1635681165488366&fexp=24001373,24007246,24138379&beids=24138379&c=WEB&txp=6210224&n=6xYjxeWrw_zjMQ&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Ccnr%2Cratebypass%2Cdur%2Clmt&sig=AOq0QJ8wRQIhAN27B47lWz5Ed8tbZbsbl1Va4OGWtB5001k1FKRdcltiAiANllZfCTVKWlGXmg88cnqWbWEutNQt8D94pRGuZpNeqA%3D%3D&redirect_counter=1&cm2rm=sn-p5qyk7s&req_id=f25a4a8a021ea3ee&cms_redirect=yes&mh=cP&mip=2402:3a80:1a5f:8f65:e06c:7baf:a67c:a3ab&mm=34&mn=sn-q4fl6nle&ms=ltu&mt=1638608200&mv=m&mvi=3&pl=48&lsparams=mh,mip,mm,mn,ms,mv,mvi,pl&lsig=AG3C_xAwRQIhAJgn46sjqWaTYhdBZ6TE0_ikoIiMYRAHKl4hFkDTd-xgAiAVTafb4Sal2K6w6JKrbktVI2NJiGCEKvLu3ScZqq5oPg%3D%3D

//audio-> https://srv4.y2mate.is/download?file=60a760dc3f38920228a4a98db5a95b48140003&token=BTaCt_FsmhGKTT0EyOEYBw&expires=1638633001886&s=vrH1Kl2PRWoT2epXEdfZSQ
//video-> https://r3---sn-qxaeeney.googlevideo.com/videoplayback?expire=1638637716&ei=NEyrYdDGFNaQ1gKes6eYAw&ip=37.120.193.252&id=o-AKj4I8SL1piJnwSDZtf734mxcsMiVPj1KPirtewodLE7&itag=243&aitags=133%2C134%2C135%2C136%2C160%2C242%2C243%2C244%2C247%2C278%2C394%2C395%2C396%2C397%2C398&source=youtube&requiressl=yes&vprv=1&mime=video%2Fwebm&ns=Z-KHHFlkcLokWivl9A9tAIEG&gir=yes&clen=16458520&dur=768.067&lmt=1540950346589247&keepalive=yes&fexp=24001373,24007246&c=WEB&txp=5432432&n=yw8oIQOopRtsiA&sparams=expire%2Cei%2Cip%2Cid%2Caitags%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cns%2Cgir%2Cclen%2Cdur%2Clmt&sig=AOq0QJ8wRAIgbvWML2Lsob-HZpbjY8bKf3y1qOzwkx9iYUK8Af80pqsCIDMLoZh5j81DmpR7q-x-5B4Yhp0070-g0EwMtak2sizc&rm=sn-nvm-cxbs7s,sn-4g5erl7s&req_id=d91435af89b4a3ee&redirect_counter=2&cms_redirect=yes&ipbypass=yes&mh=Ku&mip=2402:3a80:1a5f:8f65:e06c:7baf:a67c:a3ab&mm=29&mn=sn-qxaeeney&ms=rdu&mt=1638615650&mv=m&mvi=3&pl=48&lsparams=ipbypass,mh,mip,mm,mn,ms,mv,mvi,pl&lsig=AG3C_xAwRgIhAIvNB9TGv1oWIrsZCIui3CuDPnpibzDkK-2f3MDjGYf6AiEAusXeV1-HYmUxRfaiAjmiG3DmAObuxS6-CD8WAD1yiu0%3D
//image-> http://i.imgur.com/m1UIjW1.jpg
//text-> https://gist.github.com/protrolium/e0dbd4bb0f1a396fcb55
